<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//public blog post
Route::get('/', 'BlogController@home');
Route::get('/post/{id}/{slug}', 'BlogController@post');
Route::get('/profession/{id}', 'BlogController@profession');

//Route::get('/users', function () {
//    $allUsers = App\User::all();
//    return view('admin.index', ['users' => $allUsers]);
//});

Route::get('/users', 'UserController@index');
Route::get('/user/add', 'UserController@create');
Route::post('/user/store', 'UserController@store');

Route::get('/user/{id}', 'UserController@show');
Route::get('/user/{id}/edit', 'UserController@edit');
Route::post('/user/{id}/update', 'UserController@update');
//Route::get('/user/{id}/delete', 'UserController@destroy');
Route::post('/user/{id}/delete', 'UserController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::group(['middleware' => 'auth'], function () {
    Route::resource('professions', 'ProfessionsController');
    Route::resource('galleries', 'GalleryController');
    Route::resource('videos', 'VideoController');
    Route::resource('articles', 'ArticleController');
});

Route::get('/blog', function () {
    return view('blog');
});
