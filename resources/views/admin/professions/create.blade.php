@extends('layouts.app')

{{-- Page title --}}
@section('title', 'Create New Profession')
@section('profession_menu', 'active')

@section('content')

    <!-- /.row -->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1 class="page-header">Create New Profession</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Profession Form
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/professions', 'class' => 'form-horizontal']) !!}

                    <div class="form-group ">
                        {!! Form::label('name', 'Profession', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            {!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection