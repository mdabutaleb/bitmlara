@extends('layouts.app')
@section('title', 'Add new gallery | Dashboard ')
@section('content')
<div class="col-md-8 col-md-offset-2">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create New Article</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Article Form
                </div>
                <div class="panel-body">
                    {!! Form::open([
                    'url' => 'articles'
                    ]) !!}

                    @include('admin.articles.form')

                    {!! Form::close() !!}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@endsection