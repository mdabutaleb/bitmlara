@extends('layouts.app')
@section('title')
    {{ $article->title }}
@endsection
@section('content')

    <div class="col-md-6 col-md-offset-3">
        @foreach($errors->all() as $item)
            <div class="alert-danger col-md-6 col-md-offset-1">
                {{ $item }}
            </div>
        @endforeach
    </div>
    {{--    {{ dd($galleries) }}--}}
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <td>ID</td>
                <td>Title</td>
                <td>Sub Title</td>
                <td>Summary</td>
                <td>Details</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $article->id }}</td>
                <td>{{ $article->title }}</td>
                <td>{{ $article->sub_title }}</td>
                <td>{{ $article->summary }}</td>
                <td>{{ $article->details }}</td>
                <td>
                    <a href="{{ url('/articles/'.$article->id. '/edit') }}">Edit</a>
                </td>
            </tr>
            </tbody>


        </table>
    </div>


@endsection