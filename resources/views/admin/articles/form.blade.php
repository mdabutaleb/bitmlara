@include('admin.layouts.alert')


<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'control-label required']) !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('sub_title') ? 'has-error' : ''}}">
    {!! Form::label('sub_title', 'Sub Title', ['class' => 'control-label']) !!}
    {!! Form::text('sub_title', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('sub_title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('summary') ? 'has-error' : ''}}">
    {!! Form::label('summary', 'Summary', ['class' => 'control-label required']) !!}
    {!! Form::textarea('summary', null, ['class' => 'form-control', 'rows' => '3', 'required' => 'required']) !!}
    {!! $errors->first('summary', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
    {!! Form::label('details', 'Details', ['class' => 'control-label required']) !!}
    {!! Form::textarea('details', null, ['class' => 'form-control editorDetails', 'id' => 'editorDetails', 'rows' => '10', 'required' => 'required']) !!}
    {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('display') ? 'has-error' : ''}}">
    {!! Form::label('display', trans('articles.display'), ['class' => 'control-label required']) !!}
    <label class="radio-inline">
        {{ Form::radio('display', 'Y', true) }} Yes
    </label>
    <label class="radio-inline">
        {{ Form::radio('display', 'N') }} No
    </label>
    {!! $errors->first('display', '<p class="help-block">:message</p>') !!}
</div>

<div class="clearfix"></div>
<div class="form-group">
    {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
</div>


<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editorDetails' );
</script>