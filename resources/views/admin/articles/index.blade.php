@extends('layouts.app')
@section('title', 'Add new gallery | Dashboard ')
@section('content')

    <div class="col-md-6 col-md-offset-3">
        @foreach($errors->all() as $item)
            <div class="alert-danger col-md-6 col-md-offset-1">
                {{ $item }}
            </div>
        @endforeach
        @if(Session::has('message'))
            <div class="alert-success col-md-6 col-md-offset-1">
                {{ Session::get('message') }}
            </div>
        @endif
    </div>
    {{--    {{ dd($galleries) }}--}}
    <div class="col-md-10 col-md-offset-1">
        <table class="table table-bordered table-hover">
            <th>ID</th>
            <th>Tittle</th>
            <th>Summary</th>
            <th>Description</th>
            <th>Action</th>
            <tbody>
            @foreach($articles as $article)
                <tr>
                    <td>{{ $article->id }}</td>
                    <td class="col-md-3">{{ $article->title }}</td>
                    <td class="col-md-3">{{ $article->summary }}</td>
                    <td class="col-md-5">{{ $article->details }}</td>
                    <td>
                        <a href="{{ url('/articles/'.$article->id) }}" class="btn btn-default">View</a>
                        <a href="{{ url('/articles/'.$article->id. '/edit') }} " class="btn btn-info">Edit</a>
                        {!! Form::open(['method'=>'DELETE', 'url' => ['/galleries', $article->id],'style' => 'display:inline'
                                        ]) !!}
                        {!! Form::button('Delete', [
                            'type' => 'submit',
                            'class' => 'btn btn-danger',
                            'title' => 'Delete Gallery',
                            'onclick'=>'return confirm("Are you sure you want to delete ' . $article->name . '?")'
                       ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>


        </table>
    </div>


@endsection