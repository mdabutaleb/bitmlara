@extends('layouts.app')
@section('title', 'Add new gallery | Dashboard ')
@section('content')

    <div class="col-md-6 col-md-offset-3">
        @foreach($errors->all() as $item)
            <div class="alert-danger col-md-6 col-md-offset-1">
                {{ $item }}
            </div>
        @endforeach
    </div>
    <div class="col-md-6 col-md-offset-3">
        {!! Form::open(['method'=>'POST', 'url'=> '/videos']) !!}
        @include('admin.videos.form')
        {!! Form::close() !!}
    </div>


@endsection