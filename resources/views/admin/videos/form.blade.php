<div class="form-group">
    <div class="col-md-9">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', isset($video->title) ? $video->title : null, ['class'=> 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-9">
        {!! Form::label('summary', 'Summary') !!}
        {!! Form::textarea('summary', isset($video->summary) ? $video->summary : null, ['class'=> 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-9">
        {!! Form::label('source', 'Source') !!}
        {!! Form::text('source', isset($video->source) ? $video->source : null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-sm-9">
    {!! Form::label('provider', 'Provider : ') !!}
    @if(isset($video))
        @if($video->provider=='Y')
            {{ Form::radio('provider', 'Y', true) }} YouTube
            {{ Form::radio('provider', 'F') }} Facebook
        @else
            {{ Form::radio('provider', 'Y') }} YouTube
            {{ Form::radio('provider', 'F', true) }} Facebook
        @endif
    @else
        {{ Form::radio('provider', 'Y', true) }} YouTube
        {{ Form::radio('provider', 'F') }} Facebook
    @endif
</div>
<div class="col-sm-9">

    {!! Form::label('display', 'Display ?') !!}

    @if(isset($video))
        @if($video->display=='Y')
            {{ Form::radio('display', 'Y', true) }} Yes
            {{ Form::radio('display', 'N') }} No
        @else
            {{ Form::radio('display', 'Y') }} Yes
            {{ Form::radio('display', 'N', true) }} No
        @endif
    @else
        {{ Form::radio('display', 'Y', true) }} Yes
        {{ Form::radio('display', 'N') }} No
    @endif
</div>
@if(isset($galleries))
    <div class="form-group">
        <div class="col-md-9">
            {!! Form::label('galleries_id', 'Gallery') !!}

            @foreach($galleries as $id => $gallery)
                {{Form::checkbox('gallery_ids[]', $id, in_array($id, $selected_galleries))}}
                {{ $gallery }}
            @endforeach
        </div>
    </div>
@endif
@if(isset($video))
    {{ Form::hidden('id', isset($video->id) ? $video->id : null)}}
@endif


<div class="col-md-9">
    {!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
</div>