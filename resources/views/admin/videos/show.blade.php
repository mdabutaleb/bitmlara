@extends('layouts.app')
@section('title', 'Video | Dashboard ')
@section('content')

    <div class="col-md-6 col-md-offset-3">
        @foreach($errors->all() as $item)
            <div class="alert-danger col-md-6 col-md-offset-1">
                {{ $item }}
            </div>
        @endforeach
        @if(Session::has('message'))
            <div class="alert-success col-md-6 col-md-offset-1">
                {{ Session::get('message') }}
            </div>
        @endif
    </div>
    {{--        {{ dd($video) }}--}}
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Video Information
                </div>
                <div class="panel-body">

                    <div class=" embed-responsive embed-responsive-16by9">
                        @if($video->provider === 'F')
                            <div class="fb-video embed-responsive-item"
                                 data-href="{{ $video->source }}"
                                 data-allowfullscreen="true" data-width="500px">
                            </div>
                            {{--<div class="fb-video"--}}
                            {{--data-href="{{ $video->source }}"--}}
                            {{--data-show-text="false">--}}
                            {{--</div>--}}


                        @else
                            <iframe class="embed-responsive-item" src="{{ $video->source }}"
                                    allowfullscreen></iframe>
                        @endif
                    </div>
                    <div class="table-responsive">


                        <table class="table table-bordered table-responsive table-hover">
                            <tr>
                                <th>Gallery</th>
                                <td>
                                    @foreach($galleries as $gallery)
                                        <a href="{{ url('galleries', [$gallery->id]) }}" class="label label-primary" target="_blank">{{ $gallery->name }}</a>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Created</th>
                                <td>{{ $video->created_at->toDayDateTimeString() }}</td>
                            </tr>
                            <tr>
                                <th>Modified</th>
                                <td>{{ $video->updated_at->toDayDateTimeString() }}</td>
                            </tr>

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection