@extends('layouts.app')
@section('title', 'Video | Dashboard ')
@section('content')

    <div class="col-md-6 col-md-offset-3">
        @foreach($errors->all() as $item)
            <div class="alert-danger col-md-6 col-md-offset-1">
                {{ $item }}
            </div>
        @endforeach
        @if(Session::has('message'))
            <div class="alert-success col-md-6 col-md-offset-1">
                {{ Session::get('message') }}
            </div>
        @endif
    </div>
    {{--    {{ dd($galleries) }}--}}
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Video List
                </div>
                <div class="panel-body">

                    <table class="table table-bordered table-hover">
                        <th>ID</th>
                        <th>Video</th>
                        <th>Title</th>
                        <th>Gallery</th>
                        <th>Action</th>
                        @foreach($videos as $video)
                            <tr>
                                <td>{{ $video->id }}</td>
                                <td width="300">
                                    <div class=" embed-responsive embed-responsive-16by9">
                                        @if($video->provider === 'F')
                                            <div class="fb-video embed-responsive-item"
                                                 data-href="{{ $video->source }}"
                                                 data-allowfullscreen="true" data-width="500px">
                                            </div>
                                        @else
                                            <iframe class="embed-responsive-item" src="{{ $video->source }}"
                                                    allowfullscreen>

                                            </iframe>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $video->title }}</td>
                                <td>
                                    @foreach($video->galleries as $gallery)
                                        <a href="{{ url('/galleries', $gallery->id) }}" class="label label-primary"> {{ $gallery['name'] }}</a>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ url('/videos/'.$video->id) }}" class="btn btn-default">View</a>
                                    <a href="{{ url('/videos/'.$video->id. '/edit') }} "
                                       class="btn btn-info">Edit</a>
                                    {!! Form::open(['method'=>'DELETE', 'url' => ['/videos', $video->id],'style' => 'display:inline'
                                                    ]) !!}
                                    {!! Form::button('Delete', [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger',
                                        'title' => 'Delete Gallery',
                                        'onclick'=>'return confirm("Are you sure you want to delete ' . $video->name . '?")'
                                   ]) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </table>

                </div>

            </div>
        </div>
    </div>


@endsection