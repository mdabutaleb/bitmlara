@extends('layouts.app')
@section('title', 'Add New User')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-lg-offset-3">

                <h1> Add New Account </h1>

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>

                {!! Form::open(array('url' => '/user/store', 'class' => 'form', 'method' => 'post')) !!}

                <div class="form-group">
                    {!! Form::label('Your Name') !!}
                    {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Your name']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Your E-mail Address') !!}
                    {!! Form::email('email', null, ['class'=>'form-control','placeholder'=>'Your e-mail address']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('profession', 'Profession')  !!}
                    {!! Form::select('profession_id', $professions, null,['placeholder' => 'Select Profession ...', 'class' => 'form-control', 'required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Password') !!}
                    {!! Form::password('password',  ['required','class'=>'form-control','placeholder'=>'Password', 'class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Bio') !!}
                    {!! Form::textarea('bio', null,[ 'class'=>'form-control','placeholder'=>'Your Bio' ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Web') !!}
                    {!! Form::url('web', null,['class'=>'form-control','placeholder'=>'http://example.com']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Facebook') !!}
                    {!! Form::text('facebook',  null, ['class' => 'form-control', 'required' => 'required']) !!}

                </div>
                <div class="form-group">
                    {!! Form::label('Twitter') !!}
                    {!! Form::text('twitter',  null, ['class' => 'form-control', 'required' => 'required']) !!}

                </div>
                <div class="form-group">
                    {!! Form::label('Github') !!}
                    {!! Form::text('github',  null, ['class' => 'form-control', 'required' => 'required']) !!}

                </div>

                <div class="form-group">
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection