@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-offset-3">


                <h1> Edit Information </h1>

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach

                </ul>


                {!! Form::open(array('url' => '/user/'. $user->id. '/update', 'class' => 'form', 'method' => 'post')) !!}

                <div class="form-group">
                    {!! Form::label('Your Name') !!}
                    {!! Form::text('name',  $user->name, ['class'=>'form-control', 'placeholder'=>'Your name']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('professions') !!}

                    {!! Form::select('profession_id', $professions, isset($user->profession_id) ? $user->profession_id : 'Null') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Bio') !!}
                    {!! Form::textarea('bio', isset($user->profile['bio']) ? $user->profile->bio : 'Not Provide', [ 'class'=>'form-control','placeholder'=>'Your Bio' ]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Web') !!}
                    {!! Form::url('web', isset($user->profile['web']) ? $user->profile->web : 'Not Provide',['class'=>'form-control','placeholder'=>'http://example.com']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Facebook') !!}
                    {!! Form::text('facebook',  isset($user->profile['facebook']) ? $user->profile->facebook : 'Not Provide', ['class' => 'form-control', 'required' => 'required']) !!}

                </div>
                <div class="form-group">
                    {!! Form::label('Twitter') !!}
                    {!! Form::text('twitter',  isset($user->profile['twitter']) ? $user->profile->twitter : 'Not Provide', ['class' => 'form-control', 'required' => 'required']) !!}

                </div>
                <div class="form-group">
                    {!! Form::label('Github') !!}
                    {!! Form::text('github',  isset($user->profile['github']) ? $user->profile->github : 'Not Provide', ['class' => 'form-control', 'required' => 'required']) !!}

                </div>

                <div class="form-group">
                    {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>

    </div>
@endsection