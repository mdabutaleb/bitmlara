@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered table-hover" id="dataTables">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Web</th>
                    <th>Facebook</th>
                    <th>Github</th>
                    <th>Action</th>

                    <tr>
                        @if(!empty($user->name))
                            <td>{{ $user->name }}</td>
                        @endif
                        @if(!empty($user->email))
                            <td>{{ $user->email }}</td>
                        @endif
                        <td>
                            @if(!empty($user->profile->web))
                                <a href="{{ $user->profile->web }}"
                                   target="_blank">{{ $user->profile->web }}</a>
                            @endif
                        </td>
                        <td>
                            @if(!empty($user->profile->facebook))
                                <a href="{{ $user->profile->facebook }}"
                                   target="_blank">{{ $user->profile->facebook }}</a>
                            @endif
                        </td>


                        <td>
                            @if(!empty($user->profile->github))
                                <a href="{{ $user->profile->github }}"
                                   target="_blank">{{ $user->profile->github }}</a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ url('/user/'. $user->id . '/edit')}}">Edit</a>
                            | Delelte
                        </td>

                    </tr>

                </table>
            </div>
        </div>
    </div>
@endsection
