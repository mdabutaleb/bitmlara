@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-responsive table-bordered table-hover" id="dataTables">
                    <th>Name</th>
                    <th>Email</th>
                    {{--<th>Web</th>--}}
                    <th>Profession</th>
                    <th>Facebook</th>
                    <th>Github</th>
                    <th>Action</th>

                    @foreach($users as $user)
                        <tr>
                            @if(!empty($user->name))
                                <td>{{ $user->name }}</td>
                            @endif
                            @if(!empty($user->email))
                                <td>{{ $user->email }}</td>
                            @endif
                            <td>
                                @if(!empty($user->profession->name))
                                    {{ $user->profession->name }}
                                @else
                                    {{ 'Not Provided' }}
                                @endif
                            </td>
                            {{--<td>--}}
                            {{--@if(!empty($user->profile->web))--}}
                            {{--<a href="{{ $user->profile->web }}"--}}
                            {{--target="_blank">{{ $user->profile->web }}</a>--}}
                            {{--@endif--}}
                            {{--</td>--}}
                            <td>
                                @if(!empty($user->profile->facebook))
                                    <a href="{{ $user->profile->facebook }}"
                                       target="_blank">{{ $user->profile->facebook }}</a>
                                @endif
                            </td>


                            <td>
                                @if(!empty($user->profile->github))
                                    <a href="{{ $user->profile->github }}"
                                       target="_blank">{{ $user->profile->github }}</a>
                                @endif
                            </td>
                            <td>
                                <a href="{{ url('/user/'. $user->id)}}">View | </a>
                                <a href="{{ url('/user/'. $user->id. '/edit')}}"> Edit</a>
                                {{--<a href="{{ url('user/'. $user->id. '/delete') }}">Delete</a>--}}

                                @if($user->id !== Auth::user()->id)
                                    {!! Form::open(array('url' => '/user/'. $user->id. '/delete/', 'class' => 'form', 'method' => 'post')) !!}

                                    {!! Form::submit('Delete', ['onclick'=>'return confirm(" r u sure ' . $user->name . '?")'])!!}

                                    {!! Form::close() !!}
                                @endif
                            </td>

                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
@endsection
