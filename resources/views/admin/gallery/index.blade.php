@extends('layouts.app')
@section('title', 'Add new gallery | Dashboard ')
@section('content')

    <div class="col-md-6 col-md-offset-3">
        @foreach($errors->all() as $item)
            <div class="alert-danger col-md-6 col-md-offset-1">
                {{ $item }}
            </div>
        @endforeach
        @if(Session::has('message'))
            <div class="alert-success col-md-6 col-md-offset-1">
                {{ Session::get('message') }}
            </div>
        @endif
    </div>
    {{--    {{ dd($galleries) }}--}}
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-bordered table-hover">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Action</th>
            <tbody>
            @foreach($galleries as $gallery)
                <tr>
                    <td>{{ $gallery->id }}</td>
                    <td>{{ $gallery->name }}</td>
                    <td>{{ $gallery->description }}</td>
                    <td>
                        <a href="{{ url('/galleries/'.$gallery->id) }}" class="btn btn-default">View</a>
                        <a href="{{ url('/galleries/'.$gallery->id. '/edit') }} " class="btn btn-info">Edit</a>
                        {!! Form::open(['method'=>'DELETE', 'url' => ['/galleries', $gallery->id],'style' => 'display:inline'
                                        ]) !!}
                        {!! Form::button('Delete', [
                            'type' => 'submit',
                            'class' => 'btn btn-danger',
                            'title' => 'Delete Gallery',
                            'onclick'=>'return confirm("Are you sure you want to delete ' . $gallery->name . '?")'
                       ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>


        </table>
    </div>


@endsection