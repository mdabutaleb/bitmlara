@extends('layouts.app')
@section('title', 'Add new gallery | Dashboard ')
@section('content')

    <div class="col-md-6 col-md-offset-3">
        @foreach($errors->all() as $item)
            <div class="alert-danger col-md-6 col-md-offset-1">
                {{ $item }}
            </div>
        @endforeach
    </div>
    <div class="col-md-6 col-md-offset-3">
        {!! Form::open(['method'=>'PATCH', 'url'=> ['/galleries', $gallery->id]]) !!}
        @include('admin.gallery.form')
        {!! Form::close() !!}
    </div>

@endsection