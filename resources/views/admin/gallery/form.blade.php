<div class="form-group">
    <div class="col-md-9">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', isset($gallery->name) ? $gallery->name : null, ['class'=> 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-9">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', isset($gallery->description) ? $gallery->description : null, ['class'=> 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-9">

        {!! Form::label('display', 'Display ?') !!}
        <label class="radio-inline">
            @if(isset($gallery))
                @if($gallery->display=='Y')
                    {{ Form::radio('display', 'Y', true) }} Yes
                @else
                    {{ Form::radio('display', 'Y') }} Yes
                @endif
            @else
                {{ Form::radio('display', 'Y', true) }} Yes
            @endif

        </label>
        <label class="radio-inline">
            @if(isset($gallery))
                @if($gallery->display=='N')
                    {{ Form::radio('display', 'N', true) }} No

                @else
                    {{ Form::radio('display', 'N') }} No
                @endif

            @else
                {{ Form::radio('display', 'N') }} No
            @endif
        </label>
    </div>
</div>
@foreach($videos as $video)
    <div class="panel-body col-md-6">

        <div class=" embed-responsive embed-responsive-16by9">
            @if($video->provider === 'F')
                <div class="fb-video embed-responsive-item"
                     data-href="{{ $video->source }}"
                     data-allowfullscreen="true" data-width="500px">
                </div>
                {{--<div class="fb-video"--}}
                {{--data-href="{{ $video->source }}"--}}
                {{--data-show-text="false">--}}
                {{--</div>--}}


            @else
                <iframe class="embed-responsive-item" src="{{ $video->source }}"
                        allowfullscreen></iframe>
            @endif
        </div>

    </div>
@endforeach

<div class="col-md-8 col-md-offset-2">
    {!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
</div>