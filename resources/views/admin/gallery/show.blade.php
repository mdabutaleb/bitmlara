@extends('layouts.app')
@section('title', 'Add new gallery | Dashboard ')
@section('content')

    <div class="col-md-6 col-md-offset-3">
        @foreach($errors->all() as $item)
            <div class="alert-danger col-md-6 col-md-offset-1">
                {{ $item }}
            </div>
        @endforeach
    </div>
    {{--    {{ dd($galleries) }}--}}
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Description</td>
                <td>Action</td>
            </tr>
            </thead>

            <tbody>

            <tr>
                <td>{{ $gallery->id }}</td>
                <td>{{ $gallery->name }}</td>
                <td>{{ $gallery->description }}</td>
                <td>
                    <a href="{{ url('/galleries/'.$gallery->id. '/edit') }}">Edit</a>
                </td>
            </tr>
            </tbody>


        </table>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Video Information
                </div>
                @foreach($videos as $video)
                    <div class="panel-body col-md-6">

                        <div class=" embed-responsive embed-responsive-16by9">
                            @if($video->provider === 'F')
                                <div class="fb-video embed-responsive-item"
                                     data-href="{{ $video->source }}"
                                     data-allowfullscreen="true" data-width="500px">
                                </div>
                                {{--<div class="fb-video"--}}
                                {{--data-href="{{ $video->source }}"--}}
                                {{--data-show-text="false">--}}
                                {{--</div>--}}


                            @else
                                <iframe class="embed-responsive-item" src="{{ $video->source }}"
                                        allowfullscreen></iframe>
                            @endif
                        </div>

                    </div>
                @endforeach

            </div>
        </div>
    </div>


@endsection