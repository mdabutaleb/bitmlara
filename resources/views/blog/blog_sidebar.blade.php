<h4>Professions (has many through)</h4>
<div class="list-group">
{{--    {{ dd($professions) }}--}}
    @if(!isset($profession_id))
                @php $profession_id = 0 @endphp
    @endif
    @foreach($professions as $profession)
        <a href="{{ url('/profession/' . $profession->id) }}"
           class="list-group-item{{ $profession_id == $profession->id ? ' active' : '' }}">
            <span class="badge">{{ $profession->articles_count }}</span>
            {{ $profession->name }}
        </a>
    @endforeach
</div>