@extends('layouts.blogmaster')
{{--@section('list_menu', 'active')--}}
@section('title')
    @if(isset($article->title))
        {{ $article->title }}
    @elseif(isset($title))
        {{ $title . ' | Blogs'}}
    @else
        {{ 'All Blog Post' }}
    @endif
@endsection

@section('blog')
    <h1 class="page-header">
        @if(isset($title))
            {{ $title}}
        @endif
    </h1>
    @foreach($articles as $article)
        <!-- First Blog Post -->
        <h2>
            <a href="{{url('/post/'.$article->id.'/'. str_slug($article->title, '-'))}}">{{ $article->title }}</a>
        </h2>
        <p class="lead">
            by <a href="{{ url('/user/article/'.$article->user->id) }}">{{ $article->user['name'] }}</a>
        </p>
        <p><span class="glyphicon glyphicon-time"></span> Posted on August 28, 2013 at 10:00 PM</p>
        <hr>
        <img class="img-responsive" src="http://placehold.it/900x300" alt="">
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus
            inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis
            ipsum officiis rerum.
        </p>
        <a class="btn btn-primary" href="{{url('/post/'.$article->id.'/'. str_slug($article->title, '-'))}}">Read More
            <span
                    class="glyphicon glyphicon-chevron-right"></span>
        </a>

        <hr>
    @endforeach
@endsection

@section('pagination')
    {!! $articles->render() !!}
@endsection

{{--{{ dd($professions) }}--}}
@section('blog_sidebar')
    @include('blog.blog_sidebar')
@endsection