@extends('layouts.blogmaster')
@section('title', $article->title)
@section('blog')
    <!-- First Blog Post -->
    <h2>
        <a href="{{url('/post/'.$article->id.'/'. str_slug($article->title, '-'))}}">{{ $article->title }}</a>
    </h2>
    <p class="lead">
        by <a href="{{ url('/user/article/'.$article->user->id) }}">{{ $article->user->name }}</a>
    </p>
    <p><span class="glyphicon glyphicon-time"></span> Posted on August 28, 2013 at 10:00 PM</p>
    <hr>
    <img class="img-responsive" src="http://placehold.it/900x300" alt="">
    <hr>
    <p>{{ $article->sub_title }}</p>
    <p>{{ $article->summary }}</p>
    <p>{!! $article->details !!}</p>

    <hr>
@endsection

@section('blog_sidebar')
   @include('blog.blog_sidebar')
@endsection
