<?php
$factory->define(App\Article::class, function (Faker\Generator $faker) {

    return [
        'user_id' => $faker->randomElements(App\User::pluck('id')->toArray()),
        'title' => $faker->text(120),
        'sub_title' => $faker->sentence(5),
        'slug' => $faker->title,
        'summary' => $faker->paragraph(4),
        'display' => 'Y',
        'created_at' => $faker->dateTimeThisYear(),
    ];
});