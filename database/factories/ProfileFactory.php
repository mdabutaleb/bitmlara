<?php

$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
        'bio' => $faker->paragraph(5),
        'web' => $faker->url,
        'facebook' => 'http://facebook.com/' . $faker->unique()->firstName,
        'twitter' => 'http://twitter.com/' . $faker->unique()->firstName,
        'github' => 'http://github.com/' . $faker->unique()->firstName,
    ];
});