<?php

use App\Article;
use App\User;
use Illuminate\Database\Seeder;

class Article_Table_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::all()->each(function ($user) {
            $user->articles()->saveMany(factory(Article::class, 5)->make());
        });
    }
}
