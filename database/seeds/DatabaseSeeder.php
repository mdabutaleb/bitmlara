<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(UserTableSeeder::class);
//        $this->call(ProfessionsTableSeeder::class);
//        $this->call(GalleryAndVideoSeeder::class);
        $this->call(Article_Table_Seeder::class);
    }
}
