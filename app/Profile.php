<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['bio', 'web', 'facebook', 'twitter', 'github'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
