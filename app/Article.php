<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $fillable = ['user_id', 'title', 'sub_title', 'summary', 'details', 'slug','display', 'details'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function profession(){
        return $this->belongsTo('App\Profession');
    }
}
