<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $fillable = ['name'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function articles()
    {
        return $this->hasManyThrough('App\Article', 'App\User');
    }
}
