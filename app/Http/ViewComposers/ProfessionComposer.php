<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Profession;

class ProfessionComposer
{
    public function compose(View $view)
    {
//        $professions = Profession::whereHas('articles', function ($query) {
//            $query->where('display', 'Y')
//                ->orderBy('articles.created_at', 'desc');
//        })->get();
        $professions = Profession::has('articles')->withCount('articles')->get();

        $data = [
            'professions' => $professions,
        ];
        $view->with($data);
    }

}