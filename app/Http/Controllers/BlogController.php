<?php

namespace App\Http\Controllers;

use App\Article;
use App\Profession;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function home()
    {
        $articles = Article::where('display', 'Y')->orderBy('created_at', 'desc')->paginate(5);
//        $professions = Profession::all();
        return view('blog.blog', ['articles' => $articles]);
    }

    public function post($id, $slug = null)
    {
        $article = Article::where('display', 'Y')->find($id);
//        echo $article->user_id;
        $professions = Profession::all();
//        dd($article);
        $profession_id = 0;
        return view('blog.post', ['article' => $article, 'professions' => $professions, 'profession_id' => $profession_id]);

    }

    public function profession($id)
    {
        $pro= Profession::whereHas('articles', function ($query) {
            $query->where('display', 'Y')
                ->orderBy('articles.created_at', 'desc');
        })->findOrFail($id);
//         dd($profession);
//        $pro = Profession::find($id);
        $articles = [
            'title' => $pro->name,
            'sub_title' => '',
            'articles' => $pro->articles()->orDerBy('created_at', 'desc')->paginate(2),
            'profession_id' => $id
        ];
        return view('blog.blog', $articles);
    }
}

