<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Profession;
use Session;


class ProfessionsController extends Controller
{

    public function index()
    {
        $professions = Profession::with('user')->get();
        return view('admin.professions.index', compact('professions'));
    }

    public function users($id)
    {
        $profession = Profession::with('users')->findOrFail($id);
        $data = [
            'profession' => $profession
        ];
        return view('admin.professions.users', $data);
    }


    public function create()
    {
        return view('admin.professions.create');
    }


    public function store(Request $request)
    {
        try {
            $data = $request->only('name');
            Profession::create($data);
            Session::flash('message', 'Profession added!');
            return redirect('/professions');
        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $profession = Profession::find($id);
        return view('admin.professions.edit', compact('profession'));
    }


    public function update(Request $request, $id)
    {
        try {
            $profession = Profession::findOrFail($id);
            $data = $request->only('name');
            $profession->update($data);
            Session::flash('message', 'Profession updated!');
            return redirect('/professions');
        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }
    }


    public function destroy($id)
    {
        try {
            $profession = Profession::findOrFail($id);
            $name = $profession->name;
            $profession->delete();
            Session::flash('message', $name . ' deleted!');
            return redirect('/professions');
        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }
}