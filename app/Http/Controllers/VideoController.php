<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Requests\VideoRequest;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::with('galleries')->get();
        return view('admin.videos.index', ['videos' => $videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $galleries = Gallery::pluck('name', 'id');
//        dd($galleries);
        $selected_galleries = [];
        return view('admin.videos.create', ['galleries' => $galleries, 'selected_galleries' => $selected_galleries]);
    }

    public function store(VideoRequest $request)
    {
        $data = $request->only(['title', 'summary', 'source', 'provider', 'display']);
        $vidoe = Video::create($data);

        $gallery_ids = $request->input('gallery_ids');
        $vidoe->galleries()->attach($gallery_ids);
        Session::flash('message', 'New video added');
        return redirect('/videos');
    }


    public function show(Video $video)
    {
        $galleries = $video->galleries()->get();
        return view('admin.videos.show', ['video' => $video, 'galleries' => $galleries]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        $galleries = Gallery::pluck('name', 'id');
//        $selected_gallery = $video->galleries()->pluck('id')->toArray();
        $selected_gallery = $video->galleries()->pluck('id')->toArray();
//        dd($selected_gallery);
        return view('admin.videos.edit', ['video' => $video, 'galleries' => $galleries, 'selected_galleries' => $selected_gallery]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Video $video, VideoRequest $request)
    {
        $data = $request->only('title', 'summary', 'source', 'provider', 'display');
        $video->update($data);
        $gallery_ids = $request->input('gallery_ids');
        $video->galleries()->sync($gallery_ids);

        Session::flash('message', 'Video Updated');
        return redirect('/videos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        $video->delete();
        Session::flash('message', 'Successfully Deleted');
        return redirect('/videos');
    }
}
