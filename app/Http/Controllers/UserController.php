<?php

namespace App\Http\Controllers;

use App\Profession;
use App\Profile;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $users = User::with('profile')->get();
//        return view('admin.users.index', ['users' => $users]);
        $users = User::with('profile', 'profession')->get();
        return view('admin.users.index')->with('users', $users);
    }


    public function create()
    {
        $professions = Profession::pluck('name', 'id');
//        dd($professions);
        return view('admin.users.create')->with('professions', $professions);
    }

    public function store(Request $request)
    {
        $data = $request->only('name', 'email', 'profession_id');
        $data['password'] = password_hash($request->password, PASSWORD_BCRYPT);
        $user = User::create($data);

        $profileData = $request->only('bio', 'web', 'facebook', 'twitter', 'github');
        $user->profile()->create($profileData);
        Session::flash('message', 'User added!');
        return redirect('/users');
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.show', ['user' => $user]);
    }


    public function edit($id)
    {
        $user = User::find($id);
        $professions = Profession::pluck('name', 'id');
        return view('admin.users.edit', ['user' => $user, 'professions' => $professions]);
    }

    public function update(Request $request, $id)
    {
        $user = User::findorfail($id);
        $data = $request->only('name', 'profession_id');
        $user->update($data);

        $profileData = $request->only('bio', 'web', 'facebook', 'twitter', 'github');
        $user->profile()->update($profileData);

        Session::flash('message', 'Successfully Updated!');
        return redirect('/users');
    }

    public function destroy($id)
    {
        $user = User::findorfail($id);
        $name = $user->name;
        Session::flash('message', $name . " has been deleted");
        $user->delete();
        return redirect('/users');
    }

}
