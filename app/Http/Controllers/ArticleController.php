<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequset;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ArticleController extends Controller
{

    public function index()
    {
        $articles = Article::all()->where('user_id', Auth::user()->id);
        return view('admin.articles.index', ['articles' => $articles]);
    }


    public function create()
    {
        return view('admin.articles.create');
    }


    public function store(ArticleRequset $request)
    {
        $data = $request->only(['title', 'sub_title', 'summary', 'details', 'display']);
        $data['user_id'] = Auth::user()->id;
        $data['slug'] = str_slug($request->title, '-');
//        dd($data);
        $article = Article::create($data);
        Session::flash('message', 'Article added');
        return redirect('/articles');
    }


    public function show(Article $article)
    {
        return view('admin.articles.show', compact('article'));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
