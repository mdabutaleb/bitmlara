<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Requests\GalleryRequest;
use App\Video;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class GalleryController extends Controller
{
    public function index()
    {
        $galleries = Gallery::all();
        return view('admin.gallery.index', ['galleries' => $galleries]);
    }

    public function create(Gallery $gallery)
    {
        $videos = Video::select('id', 'provider', 'title', 'source', 'display')->get();
        $selected_vidoes = [];

        return view('admin.gallery.create', ['videos' => $videos, 'selected_videos' => $selected_vidoes]);
    }

    public function store(GalleryRequest $request)
    {
        $data = $request->only('name', 'description', 'display');
        $gallery = Gallery::create($data);

        $video_ids = $request->input('video_ids');
        $gallery->videos()->attach($video_ids);

        Session::flash('message', 'Gallery Added');
        return redirect('/galleries');
    }

    public function show(Gallery $gallery)
    {
        $videos = $gallery->videos()->get();
        return view('admin.gallery.show', ['gallery' => $gallery, 'videos' => $videos]);
    }

    public function edit(Gallery $gallery)
    {
        $videos = $gallery->videos()->get();
        $selected_video = $gallery->videos()->get();
        return view('admin.gallery.edit', ['gallery' => $gallery, 'videos' => $videos, 'selected_videos' => $selected_video]);
    }

    public function update(Gallery $gallery, GalleryRequest $request)
    {
        $data = $request->only('name', 'description', 'display');
        $gallery->update($data);

        Session::flash('message', 'Gallery Updated');
        return redirect('/galleries');
    }

    public function destroy(Gallery $gallery)
    {
        $name = $gallery->name;
        $gallery->delete();
        Session::flash('message', " $name has been deleted");
        return redirect('/galleries');
    }
}
